# saneginx - nginx container with a sane default config
this container is a normal nginx but with a (imo) saner default config that properly leverages caching and gzip compression.

repo located at https://gitlab.com/ejectedspace/docker-saneginx
